package br.edu.ifsc.crud.activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import br.edu.ifsc.crud.R;
import br.edu.ifsc.crud.adapter.AlunoAdapter;
import br.edu.ifsc.crud.model.Aluno;

public class ListaAlunos extends AppCompatActivity {
    ListView lv ;
    SQLiteDatabase bd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alunos);

        lv = findViewById(R.id.listView);
        bd = openOrCreateDatabase("alunos", MODE_PRIVATE, null);
      //  montaListagem();
    }

    public  void montaListagem(){
        Cursor registros = bd.query("alunos", null , null , null, null , null , null);
        registros.moveToFirst();

        ArrayList<Aluno> itens = new ArrayList<>();

        do {
            int codigo = registros.getInt(registros.getColumnIndex("id"));
            String nome_uc = registros.getString(registros.getColumnIndex("nome_UC"));
            Double nota = registros.getDouble(registros.getColumnIndex("nota"));
            itens.add (new Aluno(codigo, nome_uc, nota));
        } while (registros.moveToNext());

        AlunoAdapter adapter = new AlunoAdapter(this, R.layout.template_lista_aluno,itens);

        lv.setAdapter(adapter);
    }
}
